# GWR-Dengue
## GWR Dengue

## Description
This project explores de relation between dengue cases and socioeconomic variables using GWR models.

## Usage
The code is available on the file GWR_Dengue.Rmd but you can also download the html already generated and see the code and results in there.

## Support
Contact email: csoto@cenat.ac.cr

## Authors and acknowledgment
* Cristina Soto-Rojas
* Cesar Garita
* Mariela Abdalah
* Juan Gabriel Calvo
* Fabio Sanchez
* Esteban Meneses

